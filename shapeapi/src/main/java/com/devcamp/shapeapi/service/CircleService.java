package com.devcamp.shapeapi.service;

import org.springframework.stereotype.Service;

import com.devcamp.shapeapi.model.Circle;
@Service
public class CircleService {
    

    public double getCircleArea(double radius){
        return Circle.getArea(radius);

    }

    public double getCirclePerimeter(double radius){
        return Circle.getPerimeter(radius);
    }
    
    
}
