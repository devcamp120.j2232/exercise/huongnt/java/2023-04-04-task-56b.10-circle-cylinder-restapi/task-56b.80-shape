package com.devcamp.shapeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShapeapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShapeapiApplication.class, args);
	}

}
