package com.devcamp.shapeapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shapeapi.service.CircleService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CircleController {
    @Autowired
    CircleService circleService;
    @GetMapping("/circle-area")
    public double getCircleAreaApi (@RequestParam (name="radius", required = true) double radius){
        return circleService.getCircleArea(radius);

    }

    @GetMapping("/circle-perimeter")
    public double getCirclePerimeterApi (@RequestParam (name="radius", required = true) double radius){
        return circleService.getCirclePerimeter(radius);

    }
}
